package com.appium.assignment;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class LoginPage {
	
	static AppiumDriver<MobileElement> driver;

	public void login(DesiredCapabilities cap, String loginData) throws InterruptedException, MalformedURLException {

		URL url = new URL(Utility.getProperty(loginData, "url"));
		driver = new AppiumDriver<MobileElement>(url, cap);
		Thread.sleep(5000);

		System.out.println("Application started successfully.");
		driver.findElementById(Utility.getProperty(loginData, "signInButton")).click();
		Thread.sleep(2000);
		driver.findElementById(Utility.getProperty(loginData, "emailLogin")).click();
		Thread.sleep(2000);
		driver.findElementById(Utility.getProperty(loginData, "userName")).sendKeys(Utility.getProperty(loginData, "email"));
		Thread.sleep(2000);
		driver.findElementById(Utility.getProperty(loginData, "password")).sendKeys(Utility.getProperty(loginData, "passwordValue"));

		Thread.sleep(2000);
		driver.findElementById(Utility.getProperty(loginData, "loginButton")).click();
		
		Thread.sleep(2000);
		driver.findElementById(Utility.getProperty(loginData, "welcomeMessage")).toString().contentEquals(Utility.getProperty(loginData, "welcomeText"));

		System.out.println("Logged in successfully.");
		driver.quit();

	}
}
