package com.appium.assignment;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class Utility {
	public static final String PATH_SEPARATOR = File.separator;
	public final static String basePath = System.getProperties().get("basedir") + PATH_SEPARATOR + "src" + PATH_SEPARATOR + "main" + PATH_SEPARATOR + "java" + PATH_SEPARATOR;
    private static Map<String, Properties> map = new HashMap<>();
    
    public Utility(List<String> fileNames) throws IOException {
        for (String f : fileNames) {
            Properties props = new Properties();
    		InputStream input = new FileInputStream(basePath + f);
    		props.load(input);
    		map.put(f, props);
        }
    }
	
    public static String getProperty(String file, String key) {
        Properties props = map.get(file);
        if (props != null) {
            return props.getProperty(key);
        }
        return null;
    }
}