package com.appium.assignment;

import java.net.MalformedURLException;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;


public class LoginTest extends BaseTests {
	LoginPage loginPage = new LoginPage();
	BaseTests nativeAppSetup = new BaseTests();

	@Test
	@Parameters({"locatorFile"})
	public void loginTest(String loginData) throws InterruptedException, MalformedURLException {
		loginPage.login(cap, loginData);		
	}
}