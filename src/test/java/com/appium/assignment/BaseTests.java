package com.appium.assignment;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;

public class BaseTests {
	public static DesiredCapabilities cap = new DesiredCapabilities();
	public static List<String> propertyList = new ArrayList<String>();

	@BeforeSuite(alwaysRun = true)
	@Parameters({"locatorFile"})
	public void nativeAppSetup(String locatorFile) throws InterruptedException, IOException {
		propertyList.add(locatorFile);
		new Utility(propertyList);
		
		cap.setCapability("deviceName", Utility.getProperty(locatorFile, "deviceName"));
		cap.setCapability("udid", Utility.getProperty(locatorFile, "udid"));
		cap.setCapability("platformName", Utility.getProperty(locatorFile, "platformName"));
		cap.setCapability("platformVersion", Utility.getProperty(locatorFile, "platformVersion"));

		//Open Spriggy App
		cap.setCapability("appPackage", Utility.getProperty(locatorFile, "appPackage"));
		cap.setCapability("appActivity", Utility.getProperty(locatorFile, "appActivity"));

	}
}
