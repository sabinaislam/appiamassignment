# Automate login test for a native android app

## Prerequisite

- Install and setup JDK 1.8 and Maven command line
- Install and setup Appium
- Install Vysor for screen mirroring (optional)
- Install sample native app from android playstore, as an example I used Spriggy here. 


## Environment Setup

- Enable USB debugging in your android phone under Settings > Developer options  
- Update Mobile Device information in the locator.properties file as below
	
	deviceName (eg.SM-N9750), 
	udid (eg.RF8M80AKBJH), 
	platformVersion (eg.10)


## Test Execution

- Connect the Android mobile to your computer
- Run 'mvn clean test' from command prompt. Alternatively, you can also install eclipse with TestNG  and run it 


## Report

- Open index.html file from 'target\surefire-reports\html' in a browser 

